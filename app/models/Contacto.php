<?php
namespace Forms;

class Contacto extends DataBase
{

    /*
     * Constructora
     * $db 
     */
    public function __construct($db)
    {
        parent::__construct($db);
        $this->tableName = 'inbox';
    }

    public function saveContact($data)
    {
        return $this->save($data);
    }
}
