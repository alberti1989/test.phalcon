<?php
namespace DB;

/*
 * DataBase contiene los datos de conexion a la base de datos, y permite
 * inserciones y mostrar el contenido
 */
class DataBase
{
    private $db;
    
    protected $tableName;
    
    public function __construct($db)
    {
        $this->db = $db;
    }
    
    /*
     * pre: length(keys) != 0, length(keys) == length(vals)
     */
    public function save($data)
    {
        $sKeys = implode(', ', array_keys($data));
        $binders = ':' . implode(', :', array_keys($data));
        
        $query = "INSERT INTO $this->tableName ($sKeys) VALUES ($binders);";
        $state = $this->db->prepare($query);
        foreach ($data as $key => $dat) {
            $state->bindParam(":$key", $data[$key]);
        }
        try {
            $state->execute();
        } catch (Exception $e) {
            echo $e;
            return false;
        }
        return true;
    }
    
    public function getTable($tab)
    {
        return $this->db->query("SELECT * FROM $tab;")->fetchAll();
    }
}