<?php
namespace Forms;
use \Phalcon\Forms\Form;
use \Phalcon\Forms\Element\Text;
use \Phalcon\Forms\Element\TextArea;
use \Phalcon\Forms\Element\Email;
use \Phalcon\Forms\Element\Submit;
use \Phalcon\Validation\Validator\PresenceOf;
use \Phalcon\Validation\Validator\Email as EmailValidator;

/*
 * FormContact contiene los campos del formulario de contacto
 */
class FormContact extends Form
{
    public function __construct()
    {
        $email = new Email("mail", array("id"=>"mail", 'maxlength' => 30, 'placeholder' => 'Type your e-mail'));
        $email->setLabel("e-mail:");
        $email->addValidator(new PresenceOf(array('message' => 'e-mail requerido')));
        $email->addValidator(new EmailValidator(array('message' => 'formato e-mail incorrecto')));
        $this->add($email);
        
        $asunto = new Text("asunto", array("id"=>"asunto", "placeholder"=>"Asunto (opcional)"));
        $asunto->setLabel("asunto:");
        $this->add($asunto);
        
        $arrCont = array("id"=>"contenido", "placeholder"=>"Texto", "cols" => "100", "rows" => "5");
        $contenido = new TextArea("contenido", $arrCont);
        $contenido->setLabel("contenido:");
        $contenido->addValidator(new PresenceOf(array('message' => 'rellenar contenido')));
        $this->add($contenido);
        
        $sub = new Submit("submit", array("id"=>"submit"));
        $this->add($sub);
    }
}
