<?php
namespace Controllers;
use \Models\FormContact;
use \Models\Contacto;

class NewcontactController extends ControllerBase
{
    public function indexAction()
    {
        $form = new FormContact();
        $this->view->setVar('form', $form);
    }

    /**
     * Insert contact into database
     */
    public function insertAction()
    {
        $this->view->disable();
        $data = $this->request->getPost();
        //print_r($data);
        //print_r($this->request->getPost());
        $db = $this->getDI()->get('db');
        $c = new Contacto($db);
        $form = new FormContact();
        if ($form->isValid($data)) {
            if ($c->saveContact($data)) {
                echo 'Contacto insertado';
            }
        }else {
            foreach ($form->getMessages() as $message) {
                echo "$message\n";
            }
        }   
    }
}