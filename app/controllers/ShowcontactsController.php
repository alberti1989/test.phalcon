<?php
namespace Controllers;
use \Modes\DataBase;

class ShowcontactsController extends ControllerBase
{
    /*
     * Muestra el contenido de la tabla inbox
     */
    public function indexAction()
    {
        $db = new DataBase($this->getDI()->get('db'));
        $tab=$db->getTable('inbox');
        $this->view->setVar('tab', $tab);
    }
}
