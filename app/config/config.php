<?php

return new \Phalcon\Config(array(
	'database' => array(
		'adapter'  => 'mysql',
		'host'     => 'localhost',
		'username' => 'root',
		'password' => 'alberty1989',
		'name'     => 'mail',
	),
	'application' => array(
		'controllersDir' => __DIR__ . '/../../app/controllers/',
		'modelsDir'      => __DIR__ . '/../../app/models/',
		'viewsDir'       => __DIR__ . '/../../app/views/',
		'libraryDir'     => __DIR__ . '/../../app/library/',
		'layoutsDir'     => __DIR__ . '/../../app/views/layouts',
		'baseUri'        => '/test.phalcon/',
	)
));
